QuestionnaireApp.controller("questionThreeController",function($scope,dataService,$location){

    $scope.SubmitQ3 = function(question){
        dataService.setAnswer(question);
        var result = dataService.getAnsArray();
        if(result[2].three == "No"){
            dataService.saveResult();
            $location.path('/result');
        }else{
            $location.path('/question4');
        }
    }

});


