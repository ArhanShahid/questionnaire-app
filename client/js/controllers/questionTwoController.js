QuestionnaireApp.controller("questionTwoController",function($scope,dataService,$location){

    $scope.SubmitQ2 = function(question){
        dataService.setAnswer(question);

        var result = dataService.getAnsArray();
        if(result[1].two == "Yes"){
            dataService.saveResult();
            $location.path('/result');
        }else{
            $location.path('/question3');
        }
    }

});

