QuestionnaireApp.factory('requestService',function(ajaxService,$rootScope){


    var signUp =  ajaxService.signUp();
    var getResult = ajaxService.getResult();



    var _reqSignUp = function(data,callback){
        signUp.save({
            user:data.user,
            ans:data.ans
        },function(res){
            callback(res)
        });
    };

    var _reqGetResult = function(callback){
        getResult.get({},function(res){
            callback(res);
        })
    };

    return{
        reqSignUp:_reqSignUp,
        reqGetResult:_reqGetResult
    }

});
