QuestionnaireApp.factory('ajaxService',function($resource,$rootScope){


    var _signUp = function(){
        return $resource('/api/signUp')
    };

    var _getResult = function(){
        return $resource('/api/getResult')
    };

    return{
        signUp:_signUp,
        getResult:_getResult
    }
});
