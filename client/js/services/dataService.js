QuestionnaireApp.service('dataService',function($rootScope,$localStorage,requestService,ajaxService){

    var UserObj = null;
    var AnsArray = [];

    var _setUser = function(user){
        UserObj = user;
    };
    var _getUser = function(user){
        return  UserObj;
    };
    var _setAnswer = function(ans){
        AnsArray.push(ans);
    };
    var _getAnsArray = function(){
        return AnsArray;
    };

    var _saveResult = function(){
        var data = {
            user:UserObj,
            ans:AnsArray
        };
        requestService.reqSignUp(data,function(){
        })

    };



    return{
        setUser:_setUser,
        getUser:_getUser,
        setAnswer:_setAnswer,
        getAnsArray:_getAnsArray,
        saveResult:_saveResult
    }
});

