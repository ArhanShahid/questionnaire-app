var QuestionnaireApp = angular.module('QuestionnaireApp',['ui.router','ngRoute','ngResource','ngStorage']);
QuestionnaireApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('home', {
            url:'/home',
            templateUrl: 'partials/home.html',
            controller:'homeController'
        })
        .state('question1', {
            url:'/question1',
            templateUrl: 'partials/question1.html',
            controller:'questionOneController'
        })
        .state('question2', {
            url:'/question2',
            templateUrl: 'partials/question2.html',
            controller:'questionTwoController'
        })
        .state('question3', {
            url:'/question3',
            templateUrl: 'partials/question3.html',
            controller:'questionThreeController'
        })
        .state('question4', {
            url:'/question4',
            templateUrl: 'partials/question4.html',
            controller:'questionFourController'
        })
        .state('result', {
            url:'/result',
            templateUrl: 'partials/result.html',
            controller:'resultController'
        });


    //.state('home.logIn',{
    //    url:'/login',
    //    templateUrl: 'partials/logIn.html',
    //    controller:'logInController'
    //})
    //.state('home.signUp',{
    //    templateUrl: 'partials/signUp.html',
    //    controller:'signUpController'
    //})
    //.state('contact',{
    //    url:'/contact',
    //    templateUrl: 'partials/contact.html',
    //    controller:'contactController'
    //})
    //.state('add',{
    //    url:'/add',
    //    templateUrl: 'partials/add.html',
    //    controller:'addController'
    //})
    //.state('edit',{
    //    url:'/edit',
    //    templateUrl: 'partials/edit.html',
    //    controller:'editController'
    //})
    //.state('editAccount',{
    //    url:'/editAccount',
    //    templateUrl: 'partials/editAccount.html',
    //    controller:'editAccountController'
    //})
    //.state('changePassword',{
    //    url:'/changePassword',
    //    templateUrl: 'partials/changePassword.html',
    //    controller:'changePasswordController'
    //})

});

