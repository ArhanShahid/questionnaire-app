exports = module.exports = function(app, mongoose) {

    'use strict';
    var Schema = mongoose.Schema;
    var ObjectId = mongoose.Schema.Types.ObjectId;

    var UserSchema = new Schema({
        name                    : String,
        age                     : Number,
        gender                  : String,
        email                   : String,
        contact                 : Number,
        answers                 : [{}]
    });

    app.db.model('User', UserSchema);
};
