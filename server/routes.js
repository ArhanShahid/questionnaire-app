exports = module.exports = function(app) {


    // User signUp
    app.post('/api/signUp', app.api.signUp);

    // Get Result
    app.get('/api/getResult', app.api.getResult);

    // application -------------------------------------------------------------
    app.get('*', function(req, res) {
        res.sendfile('./client/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });
};